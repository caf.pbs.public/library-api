# library-api
A project for managing books libraries

## Startup
- Install JAVA JDK 11 in your PC. Amazon Correcto recommended.
- Executar o comando:
````shell
gradle deploy
````
- Install docker-desktop.
  -  - docker-desktop can be downloaded from [docker webpage](https://www.docker.com/products/docker-desktop)
- Install elastic search, preferably without authentication. 
  - A docker is available in the project. Num terminal, posicionado na pasta do projecto "library-api", executar o seguinte comando:
````shell
docker-compose up -d es01
````
- Verificar se o Elastic Search está a correr
````shell
docker ps
````
 


### Gradle bootRun
- Activar o elastic search. Pode ser feito pelo docker presente no projecto. Depois de executar o docker-desktop executar o seguinte commando:
````shell
docker-compose up -d es01
````
- On intellij window press `gradle bootRun`. You can only run or debug.
- Or in command line run:
````shell
gradlew.bat bootRun
````

### JAR distributtion

- Executar a task gradle deploy.
- Activar o elastic search. Pode ser feito pelo docker presente no projecto. Depois de executar o docker desktop executar:
````shell
docker-compose up -d es01
````

- aceder à pasta library-api e executar o jar
````
java -jar library-api/library-0.0.1-SNAPSHOT.jar
````

### Docker
Na raiz do projecto correr o docker compose para criar um elastic search, com recurso ao ficheiro docker-compose.yml

````shell
docker-compose up -d
````
Depois de arrancar basta aceder aos recursos do projecto

## Documentation
Documentação disponível em:
````shell
http://localhost:8181/swagger-ui.html
# Self signed certificate
https://localhost:8443/swagger-ui.html
````

### API's Example
- All libraries 
````shell
curl -X GET "http://localhost:8181/v1/library" -H "accept: */*"
````

- Delete library
````shell
curl -X DELETE "http://localhost:8181/v1/library/0c4287e6-1377-43dd-a921-03a7f6bf4d1a" -H "accept: */*"
````

- Add library
````shell
curl -X POST "http://localhost:8181/v1/library" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"address\": \"My street\", \"closeTime\": \"20:00:00\", \"name\": \"Great library\", \"openDays\": \"All\", \"openTime\": \"08:00:00\"}"
````



