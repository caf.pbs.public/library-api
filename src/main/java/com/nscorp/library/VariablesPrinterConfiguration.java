package com.nscorp.library;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Configuration
public class VariablesPrinterConfiguration {

    private static final List<String> INVALID_WORDS = List.of("PASSWORD", "SECRET");

    @EventListener
    public void handleContextRefreshed(ContextRefreshedEvent event) {
        printActiveProperties((ConfigurableEnvironment) event.getApplicationContext().getEnvironment(), "applicationConfig");
        printActiveProperties((ConfigurableEnvironment) event.getApplicationContext().getEnvironment(), "systemEnvironment");
    }

    private void printActiveProperties(ConfigurableEnvironment environment, String variablesNature) {
        log.debug("*************ACTIVE APP PROPERTIES {}", variablesNature);
        List<MapPropertySource> properties = getProperties(environment, variablesNature);
        List<String> keys = getKeys(properties);
        keys.stream()
                .filter(this::isValidKey)
                .sorted()
                .forEach(key -> log.debug("{}=\"{}\"", key, environment.getProperty(key)));
        log.debug("******************************************************************************");
    }

    private List<String> getKeys(List<MapPropertySource> properties) {
        return properties.stream()
                .map(propertySource -> propertySource.getSource().keySet())
                .flatMap(Collection::stream)
                .distinct()
                .collect(Collectors.toList());
    }

    private List<MapPropertySource> getProperties(ConfigurableEnvironment environment, String variablesNature) {
        return environment.getPropertySources().stream()
                .filter(it -> it instanceof MapPropertySource && it.getName().contains(variablesNature))
                .map(MapPropertySource.class::cast)
                .collect(Collectors.toList());
    }

    private boolean isValidKey(String key) {
        return INVALID_WORDS.stream().noneMatch(key.toUpperCase()::contains);
    }
}
