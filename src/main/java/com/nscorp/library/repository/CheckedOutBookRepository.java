package com.nscorp.library.repository;

import com.nscorp.library.model.CheckedOutBook;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface CheckedOutBookRepository extends JpaRepository<CheckedOutBook, UUID> {

    List<CheckedOutBook> getByUserIdAndActive(String userId, boolean active);
}
