package com.nscorp.library.repository;

import com.nscorp.library.model.Book;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.core.SearchPage;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BookRepository extends CrudRepository<Book, String> {

    @Query(value = "{" +
            "  \"bool\": {" +
            "    \"should\": [" +
            "      {" +
            "        \"multi_match\": {" +
            "          \"query\": \"?0\"," +
            "          \"type\": \"bool_prefix\"," +
            "          \"fields\": [" +
            "            \"title^5\"," +
            "            \"title._2gram^5\"," +
            "            \"title._3gram^5\"," +
            "            \"description\"," +
            "            \"description._2gram\"," +
            "            \"description._3gram\"," +
            "            \"subjects^2\"," +
            "            \"subjects._2gram^2\"," +
            "            \"subjects._3gram^2\"," +
            "            \"subjectTime\"," +
            "            \"subjectTime._2gram\"," +
            "            \"subjectTime._3gram\"," +
            "            \"subjectPlaces\"," +
            "            \"subjectPlaces._2gram\"," +
            "            \"subjectPlaces._3gram\"," +
            "            \"subjectPeople\"," +
            "            \"subjectPeople._2gram\"," +
            "            \"subjectPeople._3gram\"" +
            "          ]" +
            "        }" +
            "      }," +
            "      {" +
            "        \"nested\": {" +
            "          \"path\": \"authors\"," +
            "          \"query\": {" +
            "            \"multi_match\": {" +
            "              \"query\": \"?0\"," +
            "              \"type\": \"bool_prefix\"," +
            "              \"fields\": [" +
            "                \"authors.name.search^3\"," +
            "                \"authors.name.search._2gram^3\"," +
            "                \"authors.name.search._3gram^3\"," +
            "                \"authors.alternateNames\"," +
            "                \"authors.alternateNames._2gram\"," +
            "                \"authors.alternateNames._3gram\"" +
            "              ]" +
            "            }" +
            "          }" +
            "        }" +
            "      }" +
            "    ]" +
            "  }" +
            "}")
    SearchPage<Book> search(String subject, Pageable pageable);

    @Query(value = "{" +
            "    \"nested\": {" +
            "      \"path\": \"authors\"," +
            "      \"query\": {" +
            "        \"multi_match\": {" +
            "          \"query\": \"?0\"," +
            "          \"type\": \"bool_prefix\"," +
            "          \"fields\": [" +
            "                  \"authors.name.search^3\"," +
            "                  \"authors.name.search._2gram^3\"," +
            "                  \"authors.name.search._3gram^3\"," +
            "                  \"authors.alternateNames\"," +
            "                  \"authors.alternateNames._2gram\"," +
            "                  \"authors.alternateNames._3gram\"" +
            "          ]" +
            "        }" +
            "      }" +
            "    }" +
            "}")
    List<Book> searchByAuthor(String author);

    @Query(value = "{" +
            "  \"multi_match\":{" +
            "    \"query\":\"?0\"," +
            "    \"type\":\"bool_prefix\"," +
            "    \"fields\": [" +
            "      \"subjects^3\"," +
            "      \"subjects._2gram^3\"," +
            "      \"subjects._3gram^3\"," +
            "      \"subjectTime\"," +
            "      \"subjectTime._2gram\"," +
            "      \"subjectTime._3gram\"," +
            "      \"subjectPlaces\"," +
            "      \"subjectPlaces._2gram\"," +
            "      \"subjectPlaces._3gram\"," +
            "      \"subjectPeople\"," +
            "      \"subjectPeople._2gram\"," +
            "      \"subjectPeople._3gram\"" +
            "    ]" +
            "  }" +
            "}")
    List<Book> searchBySubject(String subject);

    @Query(value = "{" +
            "  \"multi_match\":{" +
            "    \"query\":\"?0\"," +
            "    \"type\":\"bool_prefix\"," +
            "    \"fields\": [" +
            "      \"title\"," +
            "      \"title._2gram\"," +
            "      \"title._3gram\"" +
            "    ]" +
            "  }" +
            "}")
    List<Book> searchByTitle(String title);
}
