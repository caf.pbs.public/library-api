package com.nscorp.library.repository;

import com.nscorp.library.model.Checkout;
import com.nscorp.library.model.LibraryBook;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface CheckoutRepository extends JpaRepository<Checkout, UUID> {
    boolean existsByBookAndUserIdAndActive(LibraryBook book, String userId, boolean active);

    Optional<Checkout> getByBookAndUserIdAndActive(LibraryBook book, String userId, boolean active);

}
