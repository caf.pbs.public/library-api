package com.nscorp.library.repository;

import com.nscorp.library.model.Library;
import com.nscorp.library.model.LibraryBook;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LibraryBookRepository extends JpaRepository<LibraryBook, LibraryBook.LibraryBookId> {
    int deleteByLibrary(Library library);

    List<LibraryBook> findByLibrary(Library library);

    List<LibraryBook> findByLibrary(Library library, Pageable pageable);
}
