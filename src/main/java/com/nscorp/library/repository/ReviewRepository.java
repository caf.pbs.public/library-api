package com.nscorp.library.repository;

import com.nscorp.library.model.Review;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ReviewRepository extends JpaRepository<Review, UUID> {
    List<Review> findByIsbn(String isbn, Pageable pageable);
    int countByIsbnAndRecommended(String isbn,boolean recommended);
    Optional<Review> findByIsbnAndReviewer(String isbn,String reviewer);
}
