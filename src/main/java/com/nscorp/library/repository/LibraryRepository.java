package com.nscorp.library.repository;

import com.nscorp.library.model.Library;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

@org.springframework.stereotype.Repository
public interface LibraryRepository extends JpaRepository<Library, UUID> {
}
