package com.nscorp.library;

import com.nscorp.library.controller.BookController;
import com.nscorp.library.model.CheckedOutBook;
import com.nscorp.library.model.Library;
import com.nscorp.library.model.LibraryBook;
import com.nscorp.library.repository.CheckedOutBookRepository;
import com.nscorp.library.repository.LibraryBookRepository;
import com.nscorp.library.repository.LibraryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.domain.Example;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.apache.catalina.connector.Connector;
import org.springframework.beans.factory.annotation.Value;

import java.util.List;

@SpringBootApplication
@Slf4j
@EnableCaching
public class LibraryApplication {

    @Value("${http.port}")
    private int httpPort;

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(LibraryApplication.class, args);
    }

    // Let's configure additional connector to enable support for both HTTP and HTTPS
    @Bean
    public ServletWebServerFactory servletContainer() {
        log.info("http port"+ httpPort);
        TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory();
        tomcat.addAdditionalTomcatConnectors(createStandardConnector());
        return tomcat;
    }

    private Connector createStandardConnector() {
        Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
        connector.setPort(httpPort);
        return connector;
    }

}
