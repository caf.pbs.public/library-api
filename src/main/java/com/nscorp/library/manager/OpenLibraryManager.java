package com.nscorp.library.manager;

import com.nscorp.library.dto.openlibrary.Author;
import com.nscorp.library.dto.openlibrary.AuthorsItem;
import com.nscorp.library.dto.openlibrary.Book;
import com.nscorp.library.dto.openlibrary.Work;
import com.nscorp.library.dto.openlibrary.WorksItem;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class OpenLibraryManager {
    private static final String OPEN_LIBRARY_URL = "https://openlibrary.org";
    private final RestTemplate restTemplate = new RestTemplate();

    @Cacheable("open-library-authors")
    public Author getAuthor(AuthorsItem authorsItem) {
        return restTemplate.getForObject(OPEN_LIBRARY_URL + authorsItem.getKey() + ".json", Author.class);
    }

    @Cacheable("open-library-books")
    public Book getBook(String isbn) {
        return restTemplate.getForObject(OPEN_LIBRARY_URL + "/isbn/" + isbn + ".json", Book.class);
    }

    @Cacheable("open-library-works")
    public Work getWork(WorksItem worksItem) {
        return restTemplate.getForObject(OPEN_LIBRARY_URL + worksItem.getKey() + ".json", Work.class);
    }
}
