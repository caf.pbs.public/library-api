package com.nscorp.library.controller;

import com.nscorp.library.model.Book;
import com.nscorp.library.model.CheckedOutBook;
import com.nscorp.library.repository.BookRepository;
import com.nscorp.library.repository.CheckedOutBookRepository;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("v1/user")
@AllArgsConstructor
public class UserController {
    public static final String DEFAULT_USER_ID = "Wonderful User";
    private final CheckedOutBookRepository checkoutRepository;
    private final BookController bookController;
    private final BookRepository bookRepository;

    @GetMapping("checked-out")
    public List<CheckedOutBook> getCheckedOutBooks(@RequestParam(defaultValue = DEFAULT_USER_ID) String userId) {
        List<CheckedOutBook> checkouts = checkoutRepository.getByUserIdAndActive(userId, true);
        populateBooks(checkouts);
        return checkouts;
    }

    @GetMapping("checkout-history")
    public List<CheckedOutBook> getCheckoutHistory(@RequestParam(defaultValue = DEFAULT_USER_ID) String userId) {
        List<CheckedOutBook> checkouts = checkoutRepository.getByUserIdAndActive(userId, false);
        populateBooks(checkouts);
        return checkouts;
    }

    void populateBooks(Collection<CheckedOutBook> checkedOutBooks) {
        Set<String> bookIds = checkedOutBooks.stream().map(CheckedOutBook::getBookId).collect(Collectors.toSet());
        Map<String, Book> collect = StreamSupport.stream(bookRepository.findAllById(bookIds).spliterator(), false).collect(Collectors.toMap(Book::getIsbn, Function.identity()));
        for (CheckedOutBook checkedOutBook : checkedOutBooks) {
            checkedOutBook.setBook(collect.computeIfAbsent(checkedOutBook.getBookId(), bookController::loadBook));
        }
    }
}
