package com.nscorp.library.controller;

import com.nscorp.library.dto.TypeaheadResponse;
import com.nscorp.library.model.Author;
import com.nscorp.library.model.Book;
import com.nscorp.library.repository.BookRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchPage;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/v1/search")
@AllArgsConstructor
public class SearchController {
    private final BookRepository bookRepository;
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    List<String> getAuthors(String query) {
        try{

        return bookRepository.searchByAuthor(query).stream().flatMap(b -> b.getAuthors().stream()).filter(a -> hasAuthorName(query, a)).map(Author::getName).distinct().collect(Collectors.toList());

        } catch (RuntimeException e){
            e.printStackTrace();
            return null;
        }
    }

    List<String> getSubjects(String query) {
        String upperCaseQuery = query.toUpperCase();
        return bookRepository.searchBySubject(query).stream().flatMap(b -> Stream.of(b.getSubjects(), b.getSubjectPeople(), b.getSubjectPlaces(), b.getSubjectTimes()))
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .filter(s -> s.toUpperCase().contains(upperCaseQuery))
                .distinct().collect(Collectors.toList());
    }

    List<String> getTitles(String query) {
        return bookRepository.searchByTitle(query).stream().map(Book::getTitle).collect(Collectors.toList());
    }

    boolean hasAuthorName(String name, Author author) {
        String upperCaseName = name.toUpperCase();
        return author.getName().toUpperCase().contains(upperCaseName) || (author.getAlternateNames()!=null && author.getAlternateNames().stream().map(String::toUpperCase).anyMatch(n -> n.contains(upperCaseName)));
    }

    @GetMapping
    public List<Book> search(@RequestParam String query, @RequestParam(defaultValue = "0") int page) {
        SearchPage<Book> searchHits = bookRepository.search(query, Pageable.ofSize(10).withPage(page));
        return searchHits.stream().map(SearchHit::getContent).collect(Collectors.toList());
    }

    @GetMapping("typeahead")
    public TypeaheadResponse typeahead(@RequestParam String query) {
        TypeaheadResponse response = new TypeaheadResponse();
        List<Runnable> o = List.of(
                () -> response.setTitles(getTitles(query)),
                () -> response.setAuthors(getAuthors(query)),
                () -> response.setSubjects(getSubjects(query))
        );
        try {
            executorService.invokeAll(o.stream().map(Executors::callable).collect(Collectors.toList()));
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        return response;
    }


}
