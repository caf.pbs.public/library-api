package com.nscorp.library.controller;

import com.nscorp.library.dto.CreateReviewRequest;
import com.nscorp.library.model.Review;
import com.nscorp.library.repository.ReviewRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/v1/book/{isbn}/review")
@AllArgsConstructor
public class ReviewController {
    private final ReviewRepository reviewRepository;

    @GetMapping
    public List<Review> getReviews(@PathVariable String isbn, @RequestParam(value = "limit", defaultValue = "10") int limit) {
        PageRequest pageRequest = PageRequest.of(0, limit, Sort.by(Sort.Direction.DESC, "createdDate"));
        return reviewRepository.findByIsbn(isbn, pageRequest);
    }

    @GetMapping("recommended-count")
    public int getRecommendedCount(@PathVariable String isbn) {
        return reviewRepository.countByIsbnAndRecommended(isbn, true);
    }

    @GetMapping("self")
    public Review getOwnReview(@PathVariable String isbn, @RequestParam(value = "userId", defaultValue = UserController.DEFAULT_USER_ID) String userId) {
        return reviewRepository.findByIsbnAndReviewer(isbn, userId).orElse(null);
    }

    @PostMapping
    public Review createReview(@PathVariable String isbn, @RequestBody CreateReviewRequest request, @RequestParam(value = "userId", defaultValue = UserController.DEFAULT_USER_ID) String userId) {
        if(reviewRepository.findByIsbnAndReviewer(isbn,userId).isPresent()){
           throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"You cannot submit a multiple reviews for the same book") ;
        }
        Review review = new Review();
        BeanUtils.copyProperties(request, review);
        review.setReviewer(userId);
        review.setIsbn(isbn);
        return reviewRepository.save(review);
    }

    @PutMapping("/{id}")
    public Review updateReview(@PathVariable UUID id, @RequestBody CreateReviewRequest request, @RequestParam(value = "userId", defaultValue = UserController.DEFAULT_USER_ID) String userId, @PathVariable String isbn) {
        Review review = reviewRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Review not found"));
        if (!review.getReviewer().equals(userId)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You can't change someone else's review");
        }
        if (!review.getIsbn().equals(isbn)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ISBN and Review mismatch");
        }
        BeanUtils.copyProperties(request, review);
        return reviewRepository.save(review);
    }
}
