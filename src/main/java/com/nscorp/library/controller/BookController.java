package com.nscorp.library.controller;

import com.nscorp.library.dto.openlibrary.Work;
import com.nscorp.library.manager.OpenLibraryManager;
import com.nscorp.library.model.Author;
import com.nscorp.library.model.Book;
import com.nscorp.library.repository.BookRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("v1/book")
@AllArgsConstructor
@Slf4j
public class BookController {
    private final BookRepository bookRepository;
    private final OpenLibraryManager openLibraryManager;

    Author convertAuthor(com.nscorp.library.dto.openlibrary.Author openLibraryAuthor) {
        Author author = new Author();
        BeanUtils.copyProperties(openLibraryAuthor, author);
        if (openLibraryAuthor.getBio() != null) {
            author.setBio(openLibraryAuthor.getBio().getValue());
        }
        author.setId(openLibraryAuthor.getKey().split("/")[2]);
        return author;
    }

    List<Author> getAuthors(com.nscorp.library.dto.openlibrary.Book openLibraryBook) {
        if (openLibraryBook.getAuthors() != null) {
            return openLibraryBook.getAuthors().stream()
                    .map(openLibraryManager::getAuthor)
                    .map(this::convertAuthor)
                    .collect(Collectors.toList());
        }
        return openLibraryBook.getWorks().stream().map(openLibraryManager::getWork)
                .map(Work::getAuthors)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(openLibraryManager::getAuthor)
                .map(this::convertAuthor)
                .collect(Collectors.toList());
    }

    Work getWork(com.nscorp.library.dto.openlibrary.Book openLibraryBook) {
        return openLibraryBook.getWorks().stream().map(openLibraryManager::getWork)
                .findFirst().orElse(new Work());
    }

    @GetMapping("{isbn}")
    public Book loadBook(@PathVariable String isbn, @RequestParam(defaultValue = "true") boolean persist) {
        log.debug("Loading {}", isbn);
        Optional<Book> byId = bookRepository.findById(isbn);
        if (byId.isPresent()) {
            return byId.get();
        }
        com.nscorp.library.dto.openlibrary.Book openLibraryBook = openLibraryManager.getBook(isbn);
        Book book = new Book();
        book.setIsbn(isbn);
        BeanUtils.copyProperties(openLibraryBook, book);
        book.setAuthors(getAuthors(openLibraryBook));
        Work work = getWork(openLibraryBook);
        BeanUtils.copyProperties(work, book);
        if (persist) {
            bookRepository.save(book);
        }
        return book;
    }

    public Book loadBook(@PathVariable String isbn) {
        return loadBook(isbn, true);
    }

}
