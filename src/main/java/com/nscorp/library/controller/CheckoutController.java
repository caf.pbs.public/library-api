package com.nscorp.library.controller;

import com.nscorp.library.model.Checkout;
import com.nscorp.library.repository.CheckoutRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.UUID;

@RestController
@RequestMapping("/v1/checkout")
@AllArgsConstructor
public class CheckoutController {

    private static final int MAX_WEEKS_ALLOWED = 6;

    private final CheckoutRepository checkoutRepository;

    @PostMapping("{id}/extend")
    public Checkout extendCheckout(@PathVariable UUID id) {
        Checkout checkout = checkoutRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "No checkout with id " + id + " found"));
        if (checkout.isOverdue()) {
            checkout.setDueDate(LocalDateTime.now().plusWeeks(1));
        } else {
            checkout.setDueDate(checkout.getDueDate().plusWeeks(1));
        }
        if (Duration.between(checkout.getCreateTimestamp(), checkout.getDueDate()).toDays() / 7 > MAX_WEEKS_ALLOWED) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You can only have a book checked out for a maximum of " + MAX_WEEKS_ALLOWED + " weeks");
        }
        checkoutRepository.save(checkout);
        return checkout;
    }
}
