package com.nscorp.library.controller;

import com.nscorp.library.dto.CreateLibraryBookRequest;
import com.nscorp.library.model.Library;
import com.nscorp.library.model.LibraryBook;
import com.nscorp.library.repository.LibraryBookRepository;
import com.nscorp.library.repository.LibraryRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@AllArgsConstructor
@RestController
@RequestMapping("v1/library/{libraryId}/book")
public class LibraryBookController {
    private final LibraryBookRepository libraryBookRepository;
    private final BookController bookController;
    private final LibraryController libraryController;


    @PostMapping("{isbn}")
    public LibraryBook createBook(@RequestBody CreateLibraryBookRequest createLibraryBookRequest, @PathVariable UUID libraryId, @PathVariable String isbn) {
        if (libraryBookRepository.existsById(new LibraryBook.LibraryBookId(isbn, libraryId))) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Book already exists in library");
        }
        return saveLibraryBook(createLibraryBookRequest, libraryId, isbn);
    }

    @PutMapping("{isbn}")
    public LibraryBook updateBook(@RequestBody CreateLibraryBookRequest createLibraryBookRequest, @PathVariable UUID libraryId, @PathVariable String isbn) {
        Optional<LibraryBook> libraryBook = libraryBookRepository.findById(new LibraryBook.LibraryBookId(isbn, libraryId));
        if (libraryBook.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Book does not exist in library");
        }
        LibraryBook l = libraryBook.get();
        if ((l.getCheckedOut()) > createLibraryBookRequest.getStock()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You can not remove more books than are available");
        }
        return saveLibraryBook(createLibraryBookRequest, libraryId, isbn);
    }

    LibraryBook saveLibraryBook(CreateLibraryBookRequest createLibraryBookRequest, UUID libraryId, String isbn) {
        LibraryBook libraryBook = new LibraryBook();
        BeanUtils.copyProperties(createLibraryBookRequest, libraryBook);
        Library library = libraryController.getLibrary(libraryId);
        libraryBook.setIsbn(isbn);
        libraryBook.setLibrary(library);
        libraryBook.setBook(bookController.loadBook(isbn));
        libraryBookRepository.save(libraryBook);
        return libraryBook;
    }

    @GetMapping("{isbn}")
    public LibraryBook getBook(@PathVariable UUID libraryId, @PathVariable() String isbn) {
        LibraryBook libraryBook = libraryBookRepository.findById(new LibraryBook.LibraryBookId(isbn, libraryId)).orElseGet(() -> new LibraryBook(isbn, libraryController.getLibrary(libraryId), null, null, null));
        libraryBook.setBook(bookController.loadBook(isbn));
        return libraryBook;
    }

    @GetMapping
    public List<LibraryBook> getBooks(@PathVariable UUID libraryId, @RequestParam(required = false) Integer limit) {
        Library library = libraryController.getLibrary(libraryId);
        List<LibraryBook> libraryBooks;
        if (limit == null || limit < 1) {
            libraryBooks = libraryBookRepository.findByLibrary(library);
        } else {
            Pageable pageable = Pageable.ofSize(limit);
            libraryBooks = libraryBookRepository.findByLibrary(library, pageable);
        }
        for (LibraryBook libraryBook : libraryBooks) {
            libraryBook.setBook(bookController.loadBook(libraryBook.getIsbn()));
        }
        return libraryBooks;
    }


}
