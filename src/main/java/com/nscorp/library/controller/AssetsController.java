package com.nscorp.library.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Files;
import java.nio.file.Path;

@RestController
@RequestMapping("/v1/assets")
@Slf4j
public class AssetsController {

    private static final String OPEN_LIBRARY_COVER_URL = "https://covers.openlibrary.org/b/isbn/";
    private static final String LOCAL_IMAGE_DIRECTORY = "/tmp/covers";
    private static final HttpClient HTTP_CLIENT = HttpClient.newBuilder().followRedirects(HttpClient.Redirect.ALWAYS).build();

    @GetMapping("cover/{image}")
    public ResponseEntity<byte[]> getCover(@PathVariable String image) {
        Path directoryPath = Path.of(LOCAL_IMAGE_DIRECTORY);
            try {
                Files.createDirectories(directoryPath);
            } catch (IOException e) {
                throw new RuntimeException("Error creating directory to save images",e);
            }

        Path imagePath = Path.of(LOCAL_IMAGE_DIRECTORY, image);
        if(!Files.exists(imagePath)){
            try {
                HTTP_CLIENT.send(HttpRequest.newBuilder(URI.create(OPEN_LIBRARY_COVER_URL+image)).GET().build(), HttpResponse.BodyHandlers.ofFile(imagePath));
            } catch (IOException e) {
                throw new RuntimeException("Error getting image from CDN",e);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                return null;
            }
        }
        try {
            return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(Files.readAllBytes(imagePath));
        } catch (IOException e) {
            throw new RuntimeException("Error getting local image",e);
        }
    }
}
