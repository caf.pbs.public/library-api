package com.nscorp.library.controller;

import com.nscorp.library.dto.CreateLibraryRequest;
import com.nscorp.library.model.Library;
import com.nscorp.library.repository.LibraryBookRepository;
import com.nscorp.library.repository.LibraryRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@RestController()
@RequestMapping(path = "/v1/library")
@Slf4j
public class LibraryController {

    private final LibraryRepository libraryRepository;
    private final LibraryBookRepository libraryBookRepository;


    @PostMapping
    public Library create(@RequestBody CreateLibraryRequest request) {
        Library library = new Library();
        BeanUtils.copyProperties(request, library);
        libraryRepository.save(library);
        return library;
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @Transactional()
    public void deleteLibrary(@PathVariable("id") UUID id) {
        Library library = getLibrary(id);
        log.info("Deleted {} books from library {}", libraryBookRepository.deleteByLibrary(library), id);
        libraryRepository.delete(library);
    }

    @GetMapping
    public List<Library> getLibraries() {
        return libraryRepository.findAll();
    }

    @GetMapping("{id}")
    public Library getLibrary(@PathVariable("id") UUID id) {
        return libraryRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Library not found"));
    }


    @PutMapping("{id}")
    public Library update(@RequestBody CreateLibraryRequest request,@PathVariable("id") UUID id) {
        Library library = getLibrary(id);
        BeanUtils.copyProperties(request, library);
        libraryRepository.save(library);
        return library;
    }
}
