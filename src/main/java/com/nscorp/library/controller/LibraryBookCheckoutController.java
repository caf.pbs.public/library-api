package com.nscorp.library.controller;

import com.nscorp.library.model.Checkout;
import com.nscorp.library.model.Library;
import com.nscorp.library.model.LibraryBook;
import com.nscorp.library.repository.CheckoutRepository;
import com.nscorp.library.repository.LibraryBookRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.UUID;

@RestController
@RequestMapping("/v1/library/{libraryId}/book/{bookId}")
@AllArgsConstructor
public class LibraryBookCheckoutController {
    private final LibraryBookRepository libraryBookRepository;
    private final CheckoutRepository checkoutRepository;

    @PostMapping("checkin")
    public void checkin(@PathVariable UUID libraryId, @PathVariable() String bookId, @RequestParam(defaultValue = UserController.DEFAULT_USER_ID) String userId) {
        LibraryBook book = libraryBookRepository.findById(new LibraryBook.LibraryBookId(bookId, libraryId)).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Book not found"));
        if(!book.getLibrary().isOpen()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"The library is currently closed, please return at "+ Library.TIME_FORMATTER.format(book.getLibrary().getOpenTime()));
        }
        Checkout checkout = checkoutRepository.getByBookAndUserIdAndActive(book, userId, true).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Book not checked out"));
        checkout.setActive(false);
        checkoutRepository.save(checkout);
    }

    @PostMapping("checkout")
    public Checkout checkout(@PathVariable UUID libraryId, @PathVariable() String bookId, @RequestParam(defaultValue = UserController.DEFAULT_USER_ID) String userId) {
        LibraryBook book = libraryBookRepository.findById(new LibraryBook.LibraryBookId(bookId, libraryId)).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Book not found"));
        if(!book.getLibrary().isOpen()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"The library is currently closed, please return at "+ Library.TIME_FORMATTER.format(book.getLibrary().getOpenTime()));
        }
        if (book.getAvailable() <= 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Not enough copies of this available");
        }
        if (checkoutRepository.existsByBookAndUserIdAndActive(book, userId, true)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You've already checked out this book");
        }
        Checkout checkout = Checkout.of(book,userId);
        checkoutRepository.save(checkout);
        book.setAvailable(book.getAvailable() - 1);//update the available count to include the checkout that was just made
        return checkout;
    }
}
