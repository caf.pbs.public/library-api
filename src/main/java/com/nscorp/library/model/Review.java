package com.nscorp.library.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Entity
@Table(name="REVIEW",indexes = {@Index(columnList = "ISBN,CREATEDDATE"),@Index(columnList = "ISBN,REVIEWER")})
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    private String reviewer;
    private String isbn;
    private boolean recommended;
    @Column(columnDefinition = "TEXT")
    private String review;
    @CreationTimestamp
    private LocalDateTime createdDate;
}
