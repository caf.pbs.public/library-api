package com.nscorp.library.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "CHECKOUT", indexes = {
        @Index(columnList = "LIBRARY_ID,BOOK_ID,ACTIVE"),
        @Index(columnList = "LIBRARY_ID,BOOK_ID,USERID,ACTIVE"),
        @Index(columnList = "USERID,ACTIVE")
})
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Checkout {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    @ManyToOne(cascade = CascadeType.DETACH, optional = false)
    @JoinColumns({
            @JoinColumn(name = "BOOK_ID", referencedColumnName = "BOOK_ID"),
            @JoinColumn(name = "LIBRARY_ID", referencedColumnName = "LIBRARY_ID")
    })
    private LibraryBook book;
    private String userId;
    private boolean active;
    private LocalDateTime dueDate;
    @CreationTimestamp
    private LocalDateTime createTimestamp;
    @UpdateTimestamp
    private LocalDateTime updateTimestamp;

    public static Checkout of(LibraryBook libraryBook,String userId){
        Checkout checkout = new Checkout();
        checkout.setBook(libraryBook);
        checkout.setUserId(userId);
        checkout.setActive(true);
        checkout.setDueDate(LocalDateTime.now().plusWeeks(3));
        return checkout;
    }


    public boolean isOverdue(){
        return dueDate.isBefore(LocalDateTime.now());
    }

}
