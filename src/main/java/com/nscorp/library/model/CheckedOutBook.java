package com.nscorp.library.model;

import lombok.Data;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.UUID;

@Entity
@Immutable
@Subselect("select c.id as ID,c.book_id BOOK_ID,c.library_id as LIBRARY_ID,c.user_id as USER_ID,c.active as ACTIVE,c.due_date AS DUE_DATE,c.create_timestamp as CREATE_TIMESTAMP,c.update_timestamp as UPDATE_TIMESTAMP," +
        "l.name as LIBRARY_NAME,l.address as LIBRARY_ADDRESS,l.open_time as LIBRARY_OPEN_TIME,l.close_time as LIBRARY_CLOSE_TIME " +
        "from CHECKOUT c join LIBRARY l on c.LIBRARY_ID=l.ID")
@Data
public class CheckedOutBook {
   @Id
   private UUID id;
   private String bookId;
   private String libraryId;
   private String userId;
   private boolean active;
   private LocalDateTime dueDate;
   private LocalDateTime createTimestamp;
   private LocalDateTime updateTimestamp;


   private String libraryName;
   private String libraryAddress;
   private LocalTime libraryOpenTime;
   private LocalTime libraryCloseTime;

   @Transient
   private Book book;


}
