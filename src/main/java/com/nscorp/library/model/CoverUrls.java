package com.nscorp.library.model;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class CoverUrls {
    private static final String COVERS_ASSETS_URL = "/api/v1/assets/cover/";
    private final String isbn;

    public String getLargeUrl() {
        return COVERS_ASSETS_URL + isbn + "-L.jpg";
    }

    public String getMediumUrl() {
        return COVERS_ASSETS_URL + isbn + "-M.jpg";
    }

    public String getSmallUrl() {
        return COVERS_ASSETS_URL + isbn + "-S.jpg";
    }
}
