package com.nscorp.library.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.Setting;

import java.util.List;
import java.util.stream.Collectors;

@Data
@Document(indexName = "books")
@Setting(replicas = 0)
public class Book {
    @Id
    @Field(type = FieldType.Keyword)
    private String isbn;
    @Field(type = FieldType.Search_As_You_Type)
    private String title;
    @Field(type = FieldType.Search_As_You_Type)
    private String description;
    @Field(type = FieldType.Nested)
    private List<Author> authors;
    @Field(type = FieldType.Search_As_You_Type)
    private List<String> subjects;
    @Field(type = FieldType.Search_As_You_Type)
    private List<String> subjectPeople;
    @Field(type = FieldType.Search_As_You_Type)
    private List<String> subjectTimes;
    @Field(type = FieldType.Search_As_You_Type)
    private List<String> subjectPlaces;
    @Field(type = FieldType.Date, format = {}, pattern = {"[MMMM ][d, ][yyyy]","MMM d, yyyy","yyyy [MMMM][MMM] d"})
    private String publishDate;
    private int numberOfPages;
    private String byStatement;

    public String getByStatement() {
        if (byStatement != null) {
            return byStatement;
        } else if (authors != null && !authors.isEmpty()) {
            return "by " + authors.stream().map(Author::getName).collect(Collectors.joining(", "));
        }
        return null;
    }

    @Transient
    public CoverUrls getCover(){
       return new CoverUrls(isbn) ;
    }


}
