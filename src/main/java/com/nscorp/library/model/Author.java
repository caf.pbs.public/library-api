package com.nscorp.library.model;

import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.InnerField;
import org.springframework.data.elasticsearch.annotations.MultiField;

import java.util.List;

@Data
public class Author {

    @MultiField(
            mainField = @Field(type = FieldType.Keyword, includeInParent = true),
            otherFields = {@InnerField(type = FieldType.Search_As_You_Type, suffix = "search")}
    )
    private String name;
    @Field(type = FieldType.Search_As_You_Type)
    private List<String> alternateNames;
    @Field(type = FieldType.Date, pattern = "[d ][MMMM ]yyyy")
    private String birthDate;
    @Field(type = FieldType.Date, pattern = "[d ][MMMM ]yyyy")
    private String deathDate;
    @Field(type = FieldType.Text, index = false)
    private String bio;
    private String id;

}
