package com.nscorp.library.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

@Entity
@Data
@Table(name = "LIBRARY")
@NoArgsConstructor
public class Library {

    public static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("h:mm a");

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    private String name;
    private String address;
    private LocalTime openTime;
    private LocalTime closeTime;
    private String openDays;

    public String getOpenStatement() {
        if (openTime == null || closeTime == null) {
            return "";
        }
        return "From " + TIME_FORMATTER.format(openTime) + " to " + TIME_FORMATTER.format(closeTime);
    }

    public boolean isOpen(){
        if (openTime == null || closeTime == null) {
            return false;
        }
        return LocalTime.now().isAfter(openTime) && LocalTime.now().isBefore(closeTime);
    }
}
