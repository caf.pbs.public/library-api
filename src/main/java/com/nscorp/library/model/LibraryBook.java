package com.nscorp.library.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Formula;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "LIBRARY_BOOK")
@IdClass(LibraryBook.LibraryBookId.class)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LibraryBook {

    @Id
    @Column(name = "BOOK_ID")
    private String isbn;
    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "LIBRARY_ID")
    @Id
    private Library library;

    private Integer stock;
    @Formula("STOCK - (SELECT COUNT(*) FROM CHECKOUT C WHERE C.LIBRARY_ID = LIBRARY_ID AND C.BOOK_ID = BOOK_ID AND C.ACTIVE = 1)")
    private Integer available;
    @Transient
    private Book book;

    public Integer getCheckedOut() {
        if (stock == null || available == null) {
            return null;
        }
        return stock - available;
    }


    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    public static class LibraryBookId implements Serializable {
        private String isbn;
        private UUID library;
    }
}
