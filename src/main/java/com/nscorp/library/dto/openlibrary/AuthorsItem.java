package com.nscorp.library.dto.openlibrary;

import lombok.Data;

@Data
public class AuthorsItem {

    private String key;
    private Author author;

    public String getKey() {
        if (key != null) {
            return key;
        }
        if (author != null) {
            return author.getKey();
        }
        return null;
    }
}
