package com.nscorp.library.dto.openlibrary;

import lombok.Data;

@Data
public class WorksItem {

    private String key;
}
