package com.nscorp.library.dto.openlibrary;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class Book {

    private String ocaid;
    private List<WorksItem> works;
    @JsonProperty("number_of_pages")
    private int numberOfPages;
    private List<LanguagesItem> languages;

    @JsonProperty("local_id")
    private List<String> localId;
    private Identifiers identifiers;
    private Created created;
    @JsonProperty("source_records")
    private List<String> sourceRecords;
    @JsonProperty("isbn_10")
    private List<String> isbn10;
    private String title;
    private Type type;
    @JsonProperty("isbn_13")
    private List<String> isbn13;
    private int revision;
    private Classifications classifications;
    private List<String> contributions;
    @JsonProperty("first_sentence")
    private FirstSentence firstSentence;
    private List<String> publishers;
    @JsonProperty("publish_date")
    private String publishDate;
    @JsonProperty("last_modified")
    private LastModified lastModified;
    private String key;
    private List<Integer> covers;
    private List<AuthorsItem> authors;
    @JsonProperty("latest_revision")
    private int latestRevision;
    @JsonProperty("by_statement")
    private String byStatement;
}
