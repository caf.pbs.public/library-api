package com.nscorp.library.dto.openlibrary;

import lombok.Data;

import java.util.List;

@Data
public class Identifiers {

    private List<String> goodreads;
    private List<String> librarything;
}
