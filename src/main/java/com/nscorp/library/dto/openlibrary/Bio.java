package com.nscorp.library.dto.openlibrary;

import lombok.Data;

@Data
public class Bio {

    private String type;
    private String value;
}
