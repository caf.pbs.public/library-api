package com.nscorp.library.dto.openlibrary;

import lombok.Data;

@Data
public class Type {

    private String key;
}
