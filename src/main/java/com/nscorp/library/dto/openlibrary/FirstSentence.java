package com.nscorp.library.dto.openlibrary;

import lombok.Data;

@Data
public class FirstSentence {

    private String type;
    private String value;
}
