package com.nscorp.library.dto.openlibrary;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class Author {

    @JsonProperty("created")
    private Created created;

    @JsonProperty("birth_date")
    private String birthDate;

    @JsonProperty("source_records")
    private List<String> sourceRecords;

    @JsonProperty("bio")
    private Bio bio;

    @JsonProperty("remote_ids")
    private RemoteIds remoteIds;

    @JsonProperty("personal_name")
    private String personalName;

    @JsonProperty("type")
    private Type type;

    @JsonProperty("photos")
    private List<Integer> photos;

    @JsonProperty("death_date")
    private String deathDate;

    @JsonProperty("revision")
    private int revision;

    @JsonProperty("alternate_names")
    private List<String> alternateNames;

    @JsonProperty("name")
    private String name;

    @JsonProperty("links")
    private List<LinksItem> links;

    @JsonProperty("last_modified")
    private LastModified lastModified;

    @JsonProperty("key")
    private String key;

    @JsonProperty("latest_revision")
    private int latestRevision;
}
