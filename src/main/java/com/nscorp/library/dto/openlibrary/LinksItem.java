package com.nscorp.library.dto.openlibrary;

import lombok.Data;

@Data
public class LinksItem {

    private Type type;
    private String title;
    private String url;
}
