package com.nscorp.library.dto.openlibrary;

import lombok.Data;

@Data
public class LastModified {

    private String type;
    private String value;
}
