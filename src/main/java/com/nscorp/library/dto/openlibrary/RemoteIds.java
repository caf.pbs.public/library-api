package com.nscorp.library.dto.openlibrary;

import lombok.Data;

@Data
public class RemoteIds {

    private String isni;
    private String wikidata;
    private String viaf;
}
