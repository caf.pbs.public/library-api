package com.nscorp.library.dto.openlibrary;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;

import java.util.List;

@Data
public class Work {

    @JsonProperty("subject_places")
    private List<String> subjectPlaces;
    @JsonProperty("subject_people")
    private List<String> subjectPeople;
    private Created created;
    private List<String> subjects;
    private JsonNode description;
    @JsonProperty("subject_times")
    private List<String> subjectTimes;
    private String title;
    private Type type;
    private int revision;
    @JsonProperty("last_modified")
    private LastModified lastModified;
    private String key;
    private List<Integer> covers;
    @JsonProperty("latest_revision")
    private int latestRevision;
    private List<AuthorsItem> authors;

    public String getDescription() {
        if (description == null) {
            return null;
        }
        if (description.isTextual()) {
            return description.asText();
        } else if (description.has("value")) {
            return description.get("value").asText();
        }
        throw new RuntimeException("Unable to get description for " + description);
    }
}
