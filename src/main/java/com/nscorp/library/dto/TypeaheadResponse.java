package com.nscorp.library.dto;

import lombok.Data;

import java.util.List;

@Data
public class TypeaheadResponse {
    private List<String> titles;
    private List<String> authors;
    private List<String> subjects;

}
