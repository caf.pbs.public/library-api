package com.nscorp.library.dto;

import com.nscorp.library.model.Book;
import lombok.Data;

import java.util.List;

@Data
public class SearchResponse {
    private String scrollId;
    private List<Book> content;
}
