package com.nscorp.library.dto;

import lombok.Data;

@Data
public class CreateBookRequest {
    private String isbn;
    private String title;
    private String description;
    private String author;
    private String imageUrl;
}
