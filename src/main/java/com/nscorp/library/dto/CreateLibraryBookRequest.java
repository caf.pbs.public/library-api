package com.nscorp.library.dto;

import lombok.Data;

@Data
public class CreateLibraryBookRequest {
    private int stock;
}
