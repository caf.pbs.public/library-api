package com.nscorp.library.dto;

import lombok.Data;

import java.time.LocalTime;

@Data
public class CreateLibraryRequest {
    private String name;
    private String address;
    private LocalTime openTime;
    private LocalTime closeTime;
    private String openDays;
}
