package com.nscorp.library.dto;

import lombok.Data;

@Data
public class CreateReviewRequest {
    private String review;
    private boolean recommended;
}
